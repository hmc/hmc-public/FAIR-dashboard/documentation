import {Steps, Callout} from "nextra/components";
import React from "react";

# Overview

### Structure
The *HMC Toolbox for Data Mining* follows a modularized approach, such that each step in the data harvesting workflow is a module and can be used independently.
Right now the following modules exist:
1. [Harvesters](harvesters.mdx): Collect metadata from specified sources and export them as `.xml` files to a specified output folder.
2. Data Enrichment: Enriches the collected metadata with additional information from external sources.
This includes the following sub-modules:
    1. [Metadata Extractor](dataEnrichment/metadataExtractor.mdx) which extracts metadata from `.xml` files according to the metadata schema.
    2. [Publication Validator](dataEnrichment/publicationValidator.mdx) which validates the publication type of the harvested publications.
    3. [Linked Data Finder](dataEnrichment/linkedDataFinder.mdx)  to find dataset publications that are supplements
    (or otherwise linked) to literature publications. Currently only the [ScholeXplorer](https://scholexplorer.openaire.eu/) API is used.
3. [Fair Meter](dataEnrichment/fairMeter.mdx): Evaluates  datasets found via the [Linked Data Finder](dataEnrichment/linkedDataFinder).
4. [Exporter](exporter.mdx): Exports the literature publications and related data publications to various formats.

### Configuration
As mentioned above the *HMC Toolbox for Data Mining* harvests and processes data from Helmholtz Centers.
To do this, it needs to know which centers exist and what their [OAI-PMH](https://www.openarchives.org/pmh/) endpoint is.
By default, it checks for a file named `centers.yaml`. An example file is provided in the repository. If any other file name is chosen it needs to be given in the `centers_conf` property.

### Mapping
<sup>*This paragraph handles mapping in a technical sense. Mapping metadata schemas is done in [Metadata Extractors](dataEnrichment/metadataExtractor).*</sup>
The Toolbox unifies found values for data repositories and research fields.
Both mappings are done using `YAML` files where the keys are the target value and the underlying lists are controlled vocabulary lists that are mapped to the target value.
The mapping rules for repositories are specified in `repository-mapping.yaml` while those for research fields are located in
`research-fields-mapping.yaml`.

### CLI
For providing CLI functionalities the library [Typer](https://typer.tiangolo.com/) is used.
Each module of the Toolbox can be run independently using the CLI.
Run
```
hmc-toolbox --help
```
for more detailed information which modules exist and what input they require.

### Logging
For logging functionalities the [python built-in module](https://docs.python.org/3/library/logging.html) is used and
can be configured at `toolbox/logging.conf`. There are three output levels supported: DEBUG, INFO and WARNING.
While WARNING only outputs real issues, INFO also give information about progress of the current Toolbox run.
DEBUG gives feedback to almost each step of a run and should not be used in production due to massive log output.

### Parallelization
The Toolbox supports parallelization via [async executed threads](https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor).
The number of threads used can be defined in an environment variable `MAX_THREADS` having a default of `1` (no threading).


## Disclaimer

Please note that the list of data publications obtained from data harvesting using the *HMC Toolbox for Data Mining*, as presented in the *HMC FAIR Data Dashboard* is neither complete nor entirely free of falsely identified data. If you wish to reuse the data shown in the dashboard for sensitive topics such as funding mechanisms, we highly recommend a manual review of the data. 