import HowToCite from './../components/how_to_cite.mdx'

# Documentation: HMC Dashboard on Open and FAIR Data in Helmholtz

## Introduction

The *HMC Dashboard on Open and FAIR Data in the Helmholtz Association* is a data-driven approach to monitor and improve open- and FAIR data practices in the Helmholtz Association, Germany’s largest non-university research organization. It is based on a modular pipeline to identify and evaluate data published alongside journal publications and is directly transferable and reusable by other research organizations.

The project addresses a variety of target groups ranging from the management level, research data professionals, and research data management staff at the eighteen Helmholtz centers, to providers of research data infrastructure as well as researchers who are interested in learning more about open- and FAIR data practices in the Helmholtz Association.

Three key challenges addressed with this project are:

1. Where is data in a cross-disciplinary, federated research organization published?
2. How can we identify gaps towards FAIR data?
3. How can we use this information to engage research data communities and -infrastructure?

## Structure of the project

The project is divided into two parts, represented by two project repositories and two sections in this documentation, respectively:
- The **HMC Toolbox for Data Mining** to harvest and automatically evaluate publication metadata. This part of the project is documented under the tab **[Toolbox](/toolbox)** at the top of this page. All source code of this project part is available on [GitLab](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining) and [Zenodo](https://doi.org/10.5281/zenodo.14192982).
- The **HMC FAIR Data Dashboard** to display interactive statistics of the metadata, previously harvested by the *HMC Toolbox for Data Mining*. This part is documented under the tab **[Dashboard](/dashboard)** at the top of this page. All source code of this part is available on [GitLab](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) and [Zenodo](https://doi.org/10.5281/zenodo.14192862).

![structure](/images/about.png)

## Disclaimer

Please note that the list of data publications obtained from data harvesting using the *[HMC Toolbox for Data Mining](/toolbox)*, as presented in the *[HMC FAIR Data Dashboard](/dashboard)* is affected by method-specific biases and is neither complete nor entirely free of falsely identified data. If you wish to reuse the data shown in this dashboard for sensitive topics such as funding mechanisms, we highly recommend a manual review of the data.

We also recommend careful interpretation of evaluation-results derived from automatized FAIR assessment. The FAIR principles are a set of high-level principles and applying them depends on the specific context such as discipline-specific aspects. There are various quantitative and qualitative methods to assess the FAIRness of data (see also [FAIRassist.org](https://fairassist.org/) but no definitive methodology (see [Wilkinson et al.](https://doi.org/10.5281/zenodo.7463421)). For this reason, different FAIR assessment tools can provide different scores for the same dataset. We may include alternate, complementary methodologies in future versions of this project. To illustrate the potentials of identifying systematic gaps with automated evaluation approaches, in this dashboard you can observe evaluation results obtained from [F-UJI](https://doi.org/10.5281/zenodo.4063720) as one selected approach. Both, the F-UJI framework and the underlying [metrics](https://doi.org/10.5281/zenodo.6461229) are subject of continuous development. The evaluation results can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but focus on machine-actionable aspects and are limited with respect to human-understandable and discipline-specific aspects of metadata. Evaluations results obtained from F-UJI can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but cannot truly assess how FAIR research data really is. 

<HowToCite />