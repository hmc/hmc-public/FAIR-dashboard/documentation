import {Callout, Steps, Tabs, FileTree} from 'nextra/components'

# The page "FAIR by Repository"

This subpage is designed as a repository-specific entry point to the data shown in this dashboard. It allows e.g. repository managers, data infrastructures and publishers to explore their target groups in Helmholtz, their development over time as well as averaged results resulting from an automated evaluation of Helmholtz data publications in their repository with respect to specific aspects of FAIR.

## Page Components
### Filter component
This component offers a filter option, enabling comprehensive data analysis. The data may be filtered by the name of the repository or publisher name, respectively, as illustrated in the following image:

![filter component](/images/r_filter_option.png)

**Attention**: The filter option chosen in this component are applied to all charts on this subpage of the dashboard.


### Charts components
The current page comprises four charts, each of which has been designed with a specific target user and a particular object in mind.

1. Target group analysis for the selected repository by Helmholtz research field
2. Target group analysis for the selected repository by Helmholtz centre
3. Average FAIR-scores according to F-UJI
4. Evolution of average FAIR scores in the selected repository


![Charts component](/images/r_1.png)
![Charts component](/images/r_2.png)
![Charts component](/images/r_3.png)
![Charts component](/images/r_4.png)



## Directories and files

In regard to the aforementioned page, all relevant directories and files can be found in the following locations:

<FileTree.Folder name="hmc-fair-data-dashboard" defaultOpen>
   <FileTree.Folder name="pages" defaultOpen>
        <FileTree.Folder name="partials" defaultOpen>
             <FileTree.Folder name="fair_by_repository" defaultOpen>
                <FileTree.File name="bottom_info_box.py" active/>
                <FileTree.File name="charts.py" active/>
                <FileTree.File name="filter_options.py" active/>
                <FileTree.File name="html_template.py" active/>
            </FileTree.Folder>
        </FileTree.Folder>
       <FileTree.Folder name="utility" defaultOpen>
            <FileTree.File name="db.py" active/>
           <FileTree.File name="helper.py" active/>
        </FileTree.Folder>
          <FileTree.File name="fair_by_repository.py" active/>
    </FileTree.Folder>
    <FileTree.Folder name="translations" defaultOpen>
        <FileTree.File name="repo.de.yml" active/>
        <FileTree.File name="repo.en.yml" active/>
    </FileTree.Folder>
</FileTree.Folder>



## Challenges we faced

The most challenging aspect of this page was the sunburst plot and the general and details FAIRness chart for a selected repository, which required additional attention during the data gathering and processing stages. In order to achieve the desired result, some manual curation of data was necessary.

## Page data flow
 The following five steps are currently being implemented in a generalised manner for this page.

1. Query the data at once and process it for the page
2. Copy and apply fillers on dataframe in two [callbacks](https://dash.plotly.com/basic-callbacks) of the page
3. Pass the final dataframe along the other required arguments to each chart function
4. process the charts separately accordingly
5. Return the gathered result from the callbacks

The construction of details overview of the sunburst plot can be viewed on the part entitled ['Page Data Flow of Data in Helmholtz'](/dashboard/design_decisions/data_in_helmholtz_page#page-data-flow).

The following section presents a comprehensive overview of the essential steps for all plots, with a particular focus on the construction of the **general and details FAIRness chart for a selected repository**.



<Steps>
    ### 1. Query the data at once and process it for the page
    In order to gain a deeper comprehension of the query procedure that we utilise, it is essential to initially familiarise oneself with the [data model and storage](/dashboard/design_decisions/data_model_and_storage) of our documentation.

    To the greatest extent feasible, we endeavor to utilize the queried data frame rather than querying the database on each page for the requisite data.

    ```py {1, 4-6} copy filename="pages/fair_by_repository.py"
    from .utility.db import data_is_supplemented_by_df as data_df


    data_df = data_df[data_df["Publication Year"] >= 2000]
    dataframe_rf = get_count_dataset_by_rf_repositories_center(data_df)
    dataframe_fair = get_fair_scores_by_centers_research_field(data_df)
    ```
    ### 2. Copy and apply fillers on dataframe in callback of the page
   This page contains two callback functions, reflecting the potential occurrence of two distinct sets of dynamic events.

    1. Selecting the repository from the **filter option**
    2. In addition of the **filter option** the **changing the details FAIRness switch options** for the **general and details FAIRness chart for a selected repository**

    In this case, we have tried to group the charts that should be changed according to the event.

    <Callout type="info">
A callback will be automatically triggered by Plotly Dash upon the loading of each page (at startup) or upon the occurrence of any alterations to the data in question.
</Callout>




    ```py {1-9} copy filename="pages/fair_by_repository.py"
   @callback(
    Output("graph_research_field_in_repository", "figure"),
    Output("graph_centers_in_repository", "figure"),
    Output("graph_average_FAIR_score_sunburst_in_repository", "figure"),
    Output("filter_msg_fair_by_repository_page", component_property="style"),
    Input("repository_filter", "value"),
    Input("viewport_container", "data"),
    )
    def update_graph_figures(repository, viewport_size):
        ...
    ```
    and

     ```py {1-7} copy filename="pages/fair_by_repository.py"
   @callback(
    Output("graph_average_FAIR_score_line_in_repository", "figure"),
    Input("repository_filter", "value"),
    Input("fair_mode", "value"),
    Input("viewport_container", "data"),
    )
    def update_graph_average_fair_score_figure(repository, fair_mode, viewport_size):
        ...
    ```



    ### 3. Pass the the final dataframe along the other required arguments to each chart functions
    In this section, a single function is proposed for each chart, which is designed to address the chart logic.

```py {1-3} copy filename="pages/fair_by_repository.py"
    graph_research_field_in_repository_chart = graph_research_field_in_repository(filtered_df)
    graph_centers_in_repository_chart = graph_centers_in_repository(filtered_df)
    graph_average_fair_score_sunburst_chart = graph_average_fair_score_sunburst(filtered_fair)
```

    ```py {1-3} copy filename="pages/fair_by_repository.py"
    graph_dataset_publication_fair_line_chart = graph_dataset_publication_fair_line(
        filtered_fair, fair_mode
    )
```


    ### 4. Process the charts separately accordingly
    In this section, we provide a description of the general and details plot, noting that the various chart functions are implemented in a manner that is distinct from one another.
    To achieve the desired result, we had checked the fair_mode value and plotted the graph accordingly.

    ```py {2,4,6,8,10} copy filename="pages/partials/fair_by_repository/charts.py"
def graph_dataset_publication_fair_line(filtered_fair, fair_mode):
    if fair_mode == "fair":
        ...
    elif fair_mode == "f":
        ...
    elif fair_mode == "a":
        ...
    elif fair_mode == "i":
        ...
    elif fair_mode == "r":
        ...
```





    ### 5. Return the gathered result from the callbacks
    Ultimately, the results of each function must be returned from the callbacks in order to facilitate the updating of the UI of each chart in accordance with the desired specifications.


    ```py {2-10} copy filename="pages/fair_by_repository.py"
    return (
        change_legend_according_viewport_size(
            graph_research_field_in_repository_chart, viewport_size_width
        ),
        change_legend_according_viewport_size(
            graph_centers_in_repository_chart, viewport_size_width
        ),
        change_legend_according_viewport_size(
            graph_average_fair_score_sunburst_chart, viewport_size_width
        ),
        {"display": "block"} if is_filtered else {"display": "none"},
        )
```

        ```py {1-3} copy filename="pages/fair_by_repository.py"
   return change_legend_according_viewport_size(
            graph_dataset_publication_fair_line_chart, viewport_size_width
    )
```


</Steps>