import {Callout, Steps, Tabs, FileTree} from 'nextra/components'

# The page "Assess my Data"

On this page we provide a feature for feedback on individual data DOIs based on automated evaluation results using F-UJI. Scientists or data professionals interested in concrete feedback on a specific data publication can view the test results and, for each aspect of FAIR, digest general recommendations on how to improve the FAIRness of the data. 

## Page Components
### Search component
The search component allows users to enter the data DOI and evaluate the data, as shown in the following figure.
![filter component](/images/a_search.png)


### Info and Charts components
The current page consists of a descriptive info section, five charts and four detailed recommendations to improve the FAIRness score for the given data publication, each designed with a specific target user and object in mind.

1. Descriptive info box
2. F-UJI scores as general
3. Findable with details
4. Accessible with details
5. Interoperable with details
6. Reusable with details

![info component](/images/a_info.png)
![Charts component](/images/a_c_1.png)
![Switch component](/images/a_s_1.png)
![Charts component](/images/a_c_2.png)



## Directories and files

In regard to the aforementioned page, all relevant directories and files can be found in the following locations:

<FileTree.Folder name="hmc-fair-data-dashboard" defaultOpen>
   <FileTree.Folder name="pages" defaultOpen>
        <FileTree.Folder name="partials" defaultOpen>
              <FileTree.Folder name="my_data" defaultOpen>
                <FileTree.File name="bottom_info_box.py" active/>
                <FileTree.File name="charts.py" active/>
                <FileTree.File name="filter_options.py" active/>
            </FileTree.Folder>
        </FileTree.Folder>
       <FileTree.Folder name="utility" defaultOpen>
           <FileTree.Folder name="fuji_local" defaultOpen>
                <FileTree.File name="datacite_info.py" active/>
                <FileTree.File name="fuji_conf.py" active/>
                <FileTree.File name="fuji_scorer.py" active/>
                <FileTree.File name="scholix_info.py" active/>
            </FileTree.Folder>
            <FileTree.File name="db.py" active/>
           <FileTree.File name="helper.py" active/>
        </FileTree.Folder>
          <FileTree.File name="assess_my_data.py" active/>
    </FileTree.Folder>
    <FileTree.Folder name="translations" defaultOpen>
        <FileTree.File name="my_data.de.yml" active/>
        <FileTree.File name="my_data.en.yml" active/>
    </FileTree.Folder>
</FileTree.Folder>



## Challenges we faced

User testing pointed us to the possibility to enter invalid DOIs or to enter DOIs of literature publications into the input field. Since these are neither the scope of F-UJI nor of this subpage, we implemented concrete tests to exclude these exceptions when a DOI is entered.

A diagram of the full decision tree is shown in the following.

## Activity flow of the page
The subsequent image illustrates the activity flow of the subpage 'Assess My Data'.

![Activity flow component](/images/activity_flow.png)

## Page data flow
 The following four steps are currently being implemented in a generalised manner for this page.

1. Query the data by each change in the user input box (Data DOI) through dash [callback](https://dash.plotly.com/basic-callbacks)
2. Process the page charts and info if the data is available in our local Database
3. Process the data gathering by means of SCHOLIX and Online FUJI service (Live Assessment)
4. Return the process charts and info to update the user interface (UI)


The following section presents a comprehensive overview of the essential steps for plots in this page.



<Steps>
    ### 1. Query the data by each change in the user input box (Data DOI) through dash [callback](https://dash.plotly.com/basic-callbacks)


       This page contains three callback functions, reflecting the potential occurrence of two distinct sets of dynamic events.

    1. Entering the new DOI
    3. Live assessment if applicable
    2. Switching the FAIR with details

    <Callout type="info">
A callback will be automatically triggered by Plotly Dash upon the loading of each page (at startup) or upon the occurrence of any alterations to the data in question.
</Callout>





    ```py {1-8} copy filename="pages/assess_my_data.py"
@callback(
    Output("intermediate_value", "data", allow_duplicate=True),
    Output("my_data", "children", allow_duplicate=True),
    Input("search_btn", "n_clicks"),
    State("input_on_submit", "value"),
    prevent_initial_call=True,
)
def get_clean_data_per_pid(_btn, value):
...

    ```

        ```py {1-8} copy filename="pages/assess_my_data.py"
   @callback(
    Output("intermediate_value", "data", allow_duplicate=True),
    Output("my_data", "children", allow_duplicate=True),
    Input("go_live_btn", "n_clicks"),
    State("input_on_submit", "value"),
    prevent_initial_call=True,
)
def process_live_assessment(n_clicks, value):
    ...

    ```


    ```py {1-6} copy filename="pages/assess_my_data.py"
    @callback(
    Output("graph_details_graph", "children"),
    Input("intermediate_value", "data"),
    Input("assess_my_data_fair_mode", "value"),
)
def update_details_graph(stored_data, fair_mode):
    ...

    ```
    ### 2. Process the page charts and info if the data is available in our local Database

        In order to query the data from the database according to a Digital Object Identifier (DOI), a function had already been defined and made available for use with the DOI as the argument.




    ```py {1, 4, 6, 12, 15} copy filename="pages/utility/db.py"
   def get_dataframe_per_doi(doi):
        conn = connection()
        cursor = conn.cursor()
        cursor.execute(SELECT_DATASET_PUBLICATION_BY_DOI_QUERY, doi)
        data = cursor.fetchall()
        dataframe = pd.DataFrame(data, columns=[i[0] for i in cursor.description])
        try:
            conn.commit()
            cursor.close()
            if dataframe.empty:
                print("DOI not found in database!")
                return False, dataframe

            print("DOI found in database!")
            return True, dataframe
        except pymysql.Error as err:
            print(f"could not close connection error pymysql {err.args[0]}: {err.args[1]}")
            conn.rollback()
            cursor.close()
            return None
    ```




    ### 3. Process the data gathering by means of SCHOLIX and Online FUJI service (Live Assessment)
The procedure of the live assessment is as follows:


```py {1, 4, 6} copy filename="pages/partials/my_data/charts.py"
    info = get_scholix_info(value)
    if info is None:
        ...
    elif "dataset" == info["Type"] or "collection" == info["Type"]:

        data_df = get_fuji_score(value, 1, info)
        if data_df is not None:
            ...
    else:
        ...
```


    ### 4. Return the process charts and info to update the user interface (UI)
      Ultimately, the results of each function must be returned from the callbacks in order to facilitate the updating of the UI of each chart in accordance with the desired specifications.

    ```py {1} copy filename="pages/partials/my_data/charts.py"
 return get_my_data(value, False)
```

        ```py {2,4,6,8} copy filename="pages/partials/my_data/charts.py"
        if fair_mode == "f":
            return f_chart(dataset)
        if fair_mode == "a":
            return a_chart(dataset)
        if fair_mode == "i":
            return i_chart(dataset)
        if fair_mode == "r":
            return r_chart(dataset)
```

    ```py {1} copy filename="pages/partials/my_data/charts.py"
      return get_my_data(value, True)
```




</Steps>
