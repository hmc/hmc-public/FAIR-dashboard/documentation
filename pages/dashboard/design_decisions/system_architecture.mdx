import { FileTree, Callout } from 'nextra/components'

# System Architecture

The HMC FAIR Data Dashboard comprises two projects. 

The [project](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard) is divided into two project components, which are available in separate sub-repositories:
- The [HMC Toolbox for Data Mining](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining) collects relevant information about open and FAIR data published by a research organization. It includes the harvesting of literature, the identification of linked datasets, the enrichment of metadata, the determination of F-UJI Scores, the storage of data in a database.
- The [HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) as a web interface for various target groups to interactively explore the collected data.

![structure](/images/about.png)

## Best Practices for HMC FAIR Data Dashboard

We adhere to modern software development best practices. These include:

1. **Version Control (GitLab):** We utilize version control systems to track changes, collaborate effectively, and manage code history.
2. **Separation of Concerns:** By dividing functionality into distinct modules, we enhance maintainability and reduce code repetition.
3. **Partial Views and Utility Functions:** We create reusable partial views and utility functions, promoting modularity and code efficiency.
4. **Single Source of Truth Data:** Consistency is maintained by relying on a single authoritative data source.
5. **Data Reuse for Performance:** Whenever possible, we reuse existing data to optimize performance.
6. **Efficient Queries:** We craft queries that balance performance, speed, and resource usage.
7. **YAML Files for Translations:** Using YAML files simplifies translation management and content updates.
8. **Responsive UI Design:** Our user interfaces adapt seamlessly to various devices.
9. **Using SVG Files:** SVG (Scalable Vector Graphics) files are employed wherever possible for scalable, resolution-independent graphics.
10. **Centralized Environment and Configuration Variables:** A single source for environment variables streamlines system setup and configuration.
11. **Semantic Directory Naming:** Well-chosen directory names enhance code maintenance.
12. **Containerization:** Bundling necessary packages in containers isolates the system from user space and operating systems.
13. **Pre-Commit Hooks:** We validate code quality using pre-commit hooks.
14. **Modern open software best practices:** We use the latest best practices of open software programmes such as **[REUSE](https://api.reuse.software/info/codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard)
and [Open Source Security Foundation (OpenSSF)](https://www.bestpractices.dev/en/projects/9488)**.



## Directory structures

<FileTree.Folder name="hmc-fair-data-dashboard" defaultOpen>

    <FileTree.Folder name="assets" defaultOpen>
        <FileTree.Folder name="fonts">
            <FileTree.File name="hifis-din-bold.woff" />
            <FileTree.File name="hifis-din-bold.woff2" />
            <FileTree.File name="hifis-din-condensed-bold.woff" />
            <FileTree.File name="hifis-din-condensed-bold.woff2" />
            <FileTree.File name="hifis-din-condensed.woff" />
            <FileTree.File name="hifis-din-condensed.woff2" />
            <FileTree.File name="hifis-din-exp-bold.woff" />
            <FileTree.File name="hifis-din-exp-bold.woff2" />
            <FileTree.File name="hifis-din-exp-italic.woff" />
            <FileTree.File name="hifis-din-exp-italic.woff2" />
            <FileTree.File name="hifis-din-exp.woff" />
            <FileTree.File name="hifis-din-exp.woff2" />
            <FileTree.File name="hifis-din-italic.woff" />
            <FileTree.File name="hifis-din-italic.woff2" />
            <FileTree.File name="hifis-din.woff" />
            <FileTree.File name="hifis-din.woff2" />
        </FileTree.Folder>
        <FileTree.Folder name="images" defaultOpen>
            <FileTree.Folder name="icons">
                <FileTree.File name="centers.svg" />
                <FileTree.File name="dataset.svg" />
                <FileTree.File name="info_update.svg" />
                <FileTree.File name="linkedin.svg" />
                <FileTree.File name="literature.svg" />
                <FileTree.File name="mastodon.svg" />
                <FileTree.File name="mattermost.svg" />
                <FileTree.File name="warning_sign.svg" />
                <FileTree.File name="x.svg" />
            </FileTree.Folder>
            <FileTree.Folder name="logos">
                <FileTree.File name="Dashboard_Logo.svg" />
                <FileTree.File name="Dashboard_Logo_inverted.svg" />
                <FileTree.File name="helmholtz_logo.svg" />
                <FileTree.File name="HMC_Logoicon_L.png" />
                <FileTree.File name="HMC_Logoicon_M.png" />
                <FileTree.File name="HMC_Logoicon_S.png" />
                <FileTree.File name="HMC_Logo_L.png" />
                <FileTree.File name="HMC_Logo_RGB_Blue.jpg" />
                <FileTree.File name="HMC_Logo_RGB_Blue.png" />
                <FileTree.File name="HMC_Logo_RGB_Blue.svg" />
            </FileTree.Folder>
            <FileTree.File name="about.png" />
            <FileTree.File name="Banner.png" />
            <FileTree.File name="hmc_Banner.jpg" />
            <FileTree.File name="vertical_FAIR.png" />
        </FileTree.Folder>
        <FileTree.File name="custom-script.js" />
        <FileTree.File name="favicon.ico" />
        <FileTree.File name="favicon.png" />
        <FileTree.File name="style.css" />
    </FileTree.Folder>
    <FileTree.Folder name="LICENSES" defaultOpen>
        <FileTree.File name="Apache-2.0.txt" />
        <FileTree.File name="CC-BY-4.0.txt" />
        <FileTree.File name="CC0-1.0.txt" />
        <FileTree.File name="LicenseRef-helmholtz.txt" />
        <FileTree.File name="LicenseRef-x.txt" />
        <FileTree.File name="MIT.txt" />
        <FileTree.File name="OFL-1.1-RFN.txt" />
    </FileTree.Folder>
    <FileTree.Folder name="mariadb" defaultOpen>
        <FileTree.Folder name="db">

        </FileTree.Folder>
        <FileTree.File name="Dockerfile" />
        <FileTree.File name="schema.sql" />
    </FileTree.Folder>
   <FileTree.Folder name="pages" defaultOpen>
        <FileTree.Folder name="partials">
            <FileTree.Folder name="about">
                <FileTree.File name="bottom_info_box.py" />
            </FileTree.Folder>
            <FileTree.Folder name="data_in_helmholtz">
                <FileTree.File name="bottom_info_box.py" />
                <FileTree.File name="charts.py" />
                <FileTree.File name="filter_options.py" />
                <FileTree.File name="html_template.py" />
            </FileTree.Folder>
            <FileTree.Folder name="fair_by_repository">
                <FileTree.File name="bottom_info_box.py" />
                <FileTree.File name="charts.py" />
                <FileTree.File name="filter_options.py" />
                <FileTree.File name="html_template.py" />
            </FileTree.Folder>
            <FileTree.Folder name="home">
                <FileTree.File name="bottom_info_box.py" />
                <FileTree.File name="charts.py" />
                <FileTree.File name="home_number.py" />
            </FileTree.Folder>
            <FileTree.Folder name="my_data">
                <FileTree.File name="bottom_info_box.py" />
                <FileTree.File name="charts.py" />
                <FileTree.File name="filter_options.py" />
            </FileTree.Folder>
            <FileTree.File name="banner.py" />
            <FileTree.File name="footer.py" />
            <FileTree.File name="navbar.py" />
        </FileTree.Folder>
       <FileTree.Folder name="utility">
           <FileTree.Folder name="fuji_local">
                <FileTree.File name="datacite_info.py" />
                <FileTree.File name="fuji_conf.py" />
                <FileTree.File name="fuji_scorer.py" />
                <FileTree.File name="scholix_info.py" />
            </FileTree.Folder>
            <FileTree.File name="colors.py" />
            <FileTree.File name="db.py" />
            <FileTree.File name="helper.py" />
        </FileTree.Folder>
       <FileTree.File name="about.py" />
        <FileTree.File name="assess_my_data.py" />
        <FileTree.File name="data_in_helmholtz.py" />
        <FileTree.File name="fair_by_repository.py" />
        <FileTree.File name="home.py" />
    </FileTree.Folder>
    <FileTree.Folder name="test">
        <FileTree.File name="pages_load_async_test.py" />
    </FileTree.Folder>
    <FileTree.Folder name="translations" defaultOpen>
        <FileTree.File name="about.de.yml" />
        <FileTree.File name="about.en.yml" />
        <FileTree.File name="data.de.yml" />
        <FileTree.File name="data.en.yml" />
        <FileTree.File name="faq.de.yml" />
        <FileTree.File name="faq.en.yml" />
        <FileTree.File name="home.de.yml" />
        <FileTree.File name="home.en.yml" />
        <FileTree.File name="my_data.de.yml" />
        <FileTree.File name="my_data.en.yml" />
        <FileTree.File name="nav.de.yml" />
        <FileTree.File name="nav.en.yml" />
        <FileTree.File name="repo.de.yml" />
        <FileTree.File name="repo.en.yml" />
    </FileTree.Folder>
    <FileTree.File name=".env.example" />
    <FileTree.File name=".gitignore" />
    <FileTree.File name=".gitlab-ci.yml" />
    <FileTree.File name=".pre-commit-config.yaml" />
    <FileTree.File name="app.py" />
    <FileTree.File name="CHANGELOG.md" />
    <FileTree.File name="CITATION.cff" />
    <FileTree.File name="codemeta.json" />
    <FileTree.File name="dashboard_logo.png" />
    <FileTree.File name="docker-compose.yml" />
    <FileTree.File name="Dockerfile" />
    <FileTree.File name="gunicorn.conf.py" />
    <FileTree.File name="INSTALLATION.md" />
    <FileTree.File name="LICENSE" />
    <FileTree.File name="pyproject.toml" />
    <FileTree.File name="README.md" />
    <FileTree.File name="REUSE.toml" />
    <FileTree.File name="run.sh" />
</FileTree.Folder>

<Callout type={'info'}>
    You may click on the directories in the tree structure above to explore the file structure.
    
    Note: some ``*.licence`` may also exist in the repository, containing the copyright and licence type information.
</Callout>

## Environment variables


This is the example of  `.env` file.
```wikitext copy filename=".env"
DB_CONTAINER_NAME_OR_ADDRESS=mariadb
DB_NAME=library_db_hmc
DB_USER=library_db_hmc
DB_PASSWORD=1234
SSL_CRT_FILE=ssl/local.crt
SSL_KEY_FILE=ssl/local.key
PORT=8050
MAX_YEAR=2024
OLD_MAX_YEAR=2022
FUJI_HOST=fuji_local_server
FUJI_PORT=80
FUJI_PROTOCOL=http
```

- The variable `DB_CONTAINER_NAME_OR_ADDRESS` can assume one of two values, either "localhost" or "mariadb," in accordance with the configuration specified in the docker-compose.yml file.
- The variable `DB_NAME` holds the name of the database.
- The variable `MAX_YEAR` variable holds the most recent year as an integer value to be considered for the evaluation and presentation of data in the dashbaord (e.g. `2024`). 
- The variable `OLD_MAX_YEAR` holds in integer value lower than `MAX_YEAR` (e.g. `2022`). On the Welcome-page, data points between `OLD_MAX_YEAR` and `MAX_YEAR` are connected with dashed lines to distinguish rather finalized data collection (solid lines) from ongoing data collection (dashed lines).
- The variable `FUJI_HOST` holds either the name of a local F-UJI container or the URL of an online F-UJI service.


## List of used packages
The following packages have been utilized in the current version:

### Run-time dependencies
    - dash>=2.6.2   MIT License (MIT)
    - pandas>=1.4.3 BSD License (BSD-3-Clause)
    - pymysql>=1.0.2   MIT License (MIT)
    - dash-bootstrap-components>=1.2.1  Apache Software License (Apache 2.0)
    - gunicorn>=20.1.0 MIT License (MIT)
    - python-dotenv>=0.21.0  BSD License (BSD-3-Clause)
    - requests>=2.28.2   Apache Software License (Apache 2.0)
    - python-i18n>=0.3.9  MIT License (MIT)
    - python-i18n[YAML] MIT License (MIT)




### Development dependencies
    - pre-commit
    - pytest-playwright
    - playwright


