import HowToCite from './../../components/how_to_cite.mdx'


# Documentation: HMC FAIR Data Dashboard

## Overview

The *HMC FAIR Data Dashboard* was developed by a team of the [Helmholtz Metadata Collaboration](https://helmholtz-metadaten.de/en/). The dashboard presents interactive statistics about open and FAIR data publications in the Helmholtz Association. All statistics are based on the data collected by the *HMC Toolbox for Data Mining*.


The dashboard addresses a variety of target groups ranging from the management level, research data professionals, research data management staff, to providers of research data infrastructure and researchers who are interested in improving their FAIR data practices. The dashboard therefore includes separate subpages, providing each target group an individual view on the data.

## Subpages

- The subpage **Welcome** provides a high level overview about publication numbers.
- The subpage **Data in Helmholtz** allows Helmholtz staff to explore where research data produced by specific Helmholtz centers is published, to estimate how much data is published every year and what the progression over time is. The filter menu allows to filter all data on this subpage for a specific research center.
- The subpage **Repositories** provides useful information to repository managers who are interested in analyzing the target groups of their repository or in identifying gaps in the FAIRness of the data.
- The subpage **My data** allows researchers interested in FAIR data practices to evaluate data publications and learn about how the FAIRness of research data can be improved.
- The subpage **About** allows users to find useful background information about this software project.


The HMC-instance of the dashboard is accessible at [https://fairdashboard.helmholtz-metadaten.de](https://fairdashboard.helmholtz-metadaten.de). 

The source code of all project parts is available [in a joint repository on GitLab](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard).


## Structure of the project

The project is divided into two parts, represented by two project repositories and two sections in this documentation, respectively:
- The **HMC Toolbox for Data Mining** to harvest and automatically evaluate publication metadata. This part of the project is documented under the tab **[Toolbox](/toolbox)** at the top of this page. All source code of this project part is available on [GitLab](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining) and [Zenodo](https://doi.org/10.5281/zenodo.14192982).
- HMC **FAIR Data Dashboard** to display interactive statistics of the metadata, previously harvested by the *HMC Toolbox for Data Mining*. This part is documented under the tab **[Dashboard](/dashboard)** at the top of this page. All source code of this part is available on [GitLab](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) and [Zenodo](https://doi.org/10.5281/zenodo.14192862).


## Disclaimer

Please note that the list of data publications obtained from data harvesting using the *[HMC Toolbox for Data Mining](/toolbox)*, as presented in the *[HMC FAIR Data Dashboard](/dashboard)* is affected by method-specific biases and is neither complete nor entirely free of falsely identified data. If you wish to reuse the data shown in this dashboard for sensitive topics such as funding mechanisms, we highly recommend a manual review of the data.

We also recommend careful interpretation of evaluation-results derived from automatized FAIR assessment. The FAIR principles are a set of high-level principles and applying them depends on the specific context such as discipline-specific aspects. There are various quantitative and qualitative methods to assess the FAIRness of data (see also [FAIRassist.org](https://fairassist.org/) but no definitive methodology (see [Wilkinson et al.](https://doi.org/10.5281/zenodo.7463421)). For this reason, different FAIR assessment tools can provide different scores for the same dataset. We may include alternate, complementary methodologies in future versions of this project. To illustrate the potentials of identifying systematic gaps with automated evaluation approaches, in this dashboard you can observe evaluation results obtained from [F-UJI](https://doi.org/10.5281/zenodo.4063720) as one selected approach. Both, the F-UJI framework and the underlying [metrics](https://doi.org/10.5281/zenodo.6461229) are subject of continuous development. The evaluation results can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but focus on machine-actionable aspects and are limited with respect to human-understandable and discipline-specific aspects of metadata. Evaluations results obtained from F-UJI can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but cannot truly assess how FAIR research data really is. 

<HowToCite />