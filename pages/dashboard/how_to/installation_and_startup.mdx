import JointDeployment from './../../../components/joint_deployment.mdx'
import {Steps, Callout} from "nextra/components";
import React from "react";


# Installation and startup
The following section provides a comprehensive, step-by-step guide to the installation process for developers, production environments, and users of our compiled public Docker images.

Three options are available for running the *HMC FAIR Data Dashboard* on your local machine. Although the step-by-step-instructions are given for Linux operating systems, similar ways exist for installing it on Windows and MacOS:

- **Option 1:** Installing the HMC FAIR Data Dashboard as a stand-alone app using *Python virtual environments*.
- **Option 2:** Installing the HMC FAIR Data Dashboard  as a stand-alone app using *docker compose*.
- **Option 3:** Joint deployment of the *HMC Toolbox for Data Mining* together with the *HMC FAIR Data Dashboard* using *docker compose*.

The installation instructions for all of these options can be found below.

## Option 1: Installing the HMC FAIR Data Dashboard as a stand-alone app using *Python virtual environments* (Linux)

<Steps>
### Setup of the development environment
    These are the required packages to be in your machine.
```bash copy
 sudo apt install python3-pip python3-venv git
```
### Clone the source code from the GitLab repository
Clone the [HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) public GitLab repository, by
```bash copy
 git clone https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```
or
```bash copy
 git clone git@codebase.helmholtz.cloud:hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```
### Create the virtual environments

```bash copy
cd hmc-fair-data-dashboard    # change to app directory
python3 -m venv venv        # creates a virtual environment in directory 'venv'
source venv/bin/activate    # enables the virtual environment
```
### Install the required packages

using 
```bash copy
pip install -e .
```

<Callout type={'info'}>
   A list of all packages required to locally run the dashboard can be found [here](/dashboard/design_decisions/system_architecture#list-of-used-packages)
   or in the respective section of the file `code.meta` in the repository.
</Callout>

### Configuration of environment variables
    Create `.env` file and fill it as `.env.example`
    ```bash copy
touch .env
```
    or copy the `.env.example` and then fill it with your desire variable
        ```bash copy
cp .env.example .env
```
<Callout type={'info'}>
   An example of the environment variable file can be found [here](/dashboard/design_decisions/system_architecture#environment-variables).
</Callout>

	### Make sure the dashboard has access to the database
Create `.env` file and fill it as `.env.example`

    ### Make sure the dashboard has access to F-UJI 

Access to a [F-UJI server](https://github.com/pangaea-data-publisher/fuji) is required for  running the live-assessment in the dashboard-subpage `Assess My Data`.
    If you choose to install the Dashboard via python virtual environments, you should also setup and run the F-UJI Service by your self, (see [F-UJI installation instructions](https://github.com/pangaea-data-publisher/fuji)).

    We provide the setup to run a local F-UJI server using Docker images in parallel to the dashboard in the `docker-compose.yml` file.

    <Callout type={'info'}>
> [F-UJI](https://github.com/pangaea-data-publisher/fuji) is a web service to programmatically assess the performance of digital data objects with respect to specific aspects of the FAIR principles, as based on [metrics](https://doi.org/10.5281/zenodo.3775793) developed by the [FAIRsFAIR](https://www.fairsfair.eu/) project.
> The service is used to evaluate data publications presented in the dashboard.
> -- <cite>Devaraju, A. and Huber, R. (2021). An automated solution for measuring the progress toward FAIR research data. Patterns, vol 2(11), https://doi.org/10.1016/j.patter.2021.100370`</cite>
</Callout>

### Run the dashboard
    #### Option A: Run the dashboard locally
To run locally the project, use:
```bash copy
python app.py
```
<Callout type={'info'}>
   Dash is running on `http://127.0.0.1:8050/`

   For further development, you may also exchange `app.run_server()` with `app.run_server(debug=True)` in the file `app.py`.
</Callout>

    #### Option B: Run the dashboard in production mode
    The dashboard can be installed on your production server with the same steps by running them in a `run.sh` file.

<Callout type={'info'}>
   You therefore add [gunicorn](https://gunicorn.org/) - Gunicorn 'Green Unicorn', a Python WSGI HTTP Server for UNIX. It is a pre-fork model. The Gunicorn server is broadly compatible with various web frameworks, simply implemented, light on server resources, and fairly performant.
</Callout>

```bash copy
./run.sh
```
<Callout>
  Executable permission is required for the `run.sh` file, and you can achieve it via:

```bash copy
sudo chmod +x run.sh
```

</Callout>


</Steps>

## Option 2: Installing the HMC FAIR Data Dashboard as a stand-alone app using *docker compose* (Linux)
The dashboard can be run from a docker image available in the [GitLab Container Registry](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard/container_registry/). This docker image of the most recent release is pulled automatically when deploying locally in the following way.

As described above for Option 1, running the dashboard as a stand-alone app requires access to a F-UJI server and at least a minimum setup of the database using mariadb.   

<Steps>

### Install docker and docker compose
The `HMC FAIR Data Dashboard` comes with an easy-to-use Docker container.
First you need to [install Docker](https://docs.docker.com/engine/install/) as well as [Docker Compose V2](https://docs.docker.com/compose/). 

### Clone the source code from the GitLab repository
Clone the [HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) public GitLab repository, by
```bash copy
 git clone https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```
or
```bash copy
 git clone git@codebase.helmholtz.cloud:hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard.git
```

### Configuration of environment variables
Change path to the new directory:
  ```bash copy
cd hmc-fair-data-dashboard
```
    Create `.env` file and fill it as `.env.example`
    ```bash copy
touch .env
```
    or copy the `.env.example` and then fill it with your desire variables
        ```bash copy
cp .env.example .env
```
<Callout type={'info'}>
   An example of the environment variable file can be found [here](/dashboard/design_decisions/system_architecture#environment-variables).
</Callout>


### Run the dashboard using docker compose 

```bash copy
docker compose up
```

<Callout type={'info'}>
Please note: A prerequisite for using the `HMC FAIR Data Dashboard` is a
[local instance of the F-UJI Server](https://github.com/pangaea-data-publisher/fuji#docker-based-installation) as well as a preconfigured MariaDB-database.
With the instructions given in `docker-compose.yml`, both the database as well as an instance of the F-UJI Server should be launched in parallel to the *HMC FAIR Data Dashboard*.
</Callout>

</Steps>


<JointDeployment />
