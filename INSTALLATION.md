# HMC Dashboard on Open and FAIR Data in the Helmholtz Association (Documentation)

<img src="https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard/-/raw/development/dashboard_logo.png" alt="EM Glossary Group Logo" width="200"/>


## Installation instructions

Please note: this installation guide is for local installation and execution 
of the documentation. For the installation of the HMC FAIR Data Dashboard 
and the HMC Toolbox for Data Mining please refer to https://fairdashboard.helmholtz-metadaten.de/docs.

### Quik Installation instructions

We have two options of installation and running of the ``Documentation``, which are:

- Installing and running locally via **NPM and Nodejs**
- Using Docker Containers


#### Installing and running locally via **NPM and Nodejs**

##### Install System Requirements:
Ensure you have [Node.js 18.18 or later](https://nodejs.org/en/download/package-manager) installed on your system. These are the steps you can check:

  ```bash
 node -v
  ```
 ```bash
 npm -v
  ```

##### Cloning the Repository
Clone the [documentation](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation) public GitLab repository, by
```bash
 git clone https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation.git
```
or
```bash
 git clone git@codebase.helmholtz.cloud:hmc/hmc-public/FAIR-dashboard/documentation.git
```

##### Install the packages

 ```bash
 cd documentation
 npm install
  ```


##### Run the Development Server

```bash
npm run dev
```
Note:
- Documentation will run on ``http://127.0.0.1:3000/``.


##### Build documentation for deployment:

```bash
  npm run build
```



##### Start the production server

```bash
  npm start
```
Or 
```bash
 npx serve@latest out
```
Note:

- Documentation will run on ``http://127.0.0.1:3000/``.


### Using Docker Containers

#### Cloning the Repository
Clone the [documentation](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation) public GitLab repository, by
```bash
 git clone https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation.git
```
or
```bash
 git clone git@codebase.helmholtz.cloud:hmc/hmc-public/FAIR-dashboard/documentation.git
```

#### Run the `docker-compose`
```bash
cd documentation
sudo docker-compose up
```

Note:
- Documentation will run on ``http://127.0.0.1:3005/`` and ``http://0.0.0.0:3005``.
- The `docker` and `docker-compose` should be installed on your machine.
- And for checking you can use: ``$ sudo docker --version`` or ``$ sudo docker-compose --version``, to see if they are
  already installed.



## Support and Contributing

If you are interested in contributing to the project or have any questions not answered here or in
the API documentation, please contact hmc-matter@helmholtz-berlin.de.
