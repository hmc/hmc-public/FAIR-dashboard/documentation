# HMC Dashboard on Open and FAIR Data in the Helmholtz Association

<img src="https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard/-/raw/development/dashboard_logo.png" alt="EM Glossary Group Logo" width="200"/>

## Received Badges for this Repository (Documentation)
[![pipeline status](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation/badges/development/pipeline.svg)](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation/-/commits/development)
[![Website](https://img.shields.io/website?url=https%3A%2F%2Ffairdashboard.helmholtz-metadaten.de%2Fdocs)
](https://fairdashboard.helmholtz-metadaten.de/docs)
![GitLab Contributors](https://img.shields.io/gitlab/contributors/12387?gitlab_url=https%3A%2F%2Fcodebase.helmholtz.cloud%2F)
![GitLab License](https://img.shields.io/gitlab/license/12387?gitlab_url=https%3A%2F%2Fcodebase.helmholtz.cloud)

[![REUSE status](https://api.reuse.software/badge/codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation)](https://api.reuse.software/info/codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation)

![GitLab Issues](https://img.shields.io/gitlab/issues/all/12387?gitlab_url=https%3A%2F%2Fcodebase.helmholtz.cloud)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/12387?gitlab_url=https%3A%2F%2Fcodebase.helmholtz.cloud)
![GitLab Issues](https://img.shields.io/gitlab/issues/closed/12387?gitlab_url=https%3A%2F%2Fcodebase.helmholtz.cloud)

[![Static Badge](https://img.shields.io/badge/Scientific_paper-10.5334%2Fdsj--2024--041-blue)](https://doi.org/10.5334/dsj-2024-041)


# Introduction

The *HMC Dashboard on Open and FAIR Data in the Helmholtz Association* is a data-driven approach to monitor and improve open- and FAIR data practices in the Helmholtz Association, Germany’s largest non-university research organization. It is based on a modular pipeline to identify and evaluate data published alongside journal publications and is directly transferable and reusable by others research organizations.

Three key questions addressed with this project are:

1. Where is data in a cross-disciplinary, federated research organization published?
2. How can we identify gaps towards FAIR data?
3. How can we use this information to engage research data communities and -infrastructure?

# Target groups

The project addresses a variety of target groups ranging from the management level, research data professionals, research data management staff at the eighteen Helmholtz centers, to providers of research data infrastructure and researchers who are interested in improving their FAIR data practices.

# Project repositories

The project is divided into four project repositories:
- HMC Toolbox for Data Mining (see [project repository](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining) and [software publication](https://zenodo.org/doi/10.5281/zenodo.10890273))
- HMC FAIR Data Dashboard (see [project repository](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) and [software publication](https://zenodo.org/doi/10.5281/zenodo.10890300))
- Documentation **(Current Repository)** (see [project repository](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/documentation) and https://fairdashboard.helmholtz-metadaten.de/docs)
- Production setup HMC Toolbox and HMC Dashboard (see [project repository](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/production-setup-hmc-toolbox-and-hmc-dashboard))



## Installation of Documentation

If you want to install the documentation locally, you can fallow in steps available in [INSTALLATION.md](INSTALLATION.md).

# Data availability

A selective export of the [dataset](https://zenodo.org/doi/10.5281/zenodo.10890382) is published on Zenodo and will be updated in regular intervals.

## Disclaimer

Please note that the list of data publications obtained from data harvesting using the *[HMC Toolbox for Data Mining](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-toolbox-for-data-mining)*, as presented in the *[HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard)* is affected by method-specific biases and is neither complete nor entirely free of falsely identified data. If you wish to reuse the data shown in this dashboard for sensitive topics such as funding mechanisms, we highly recommend a manual review of the data.

We also recommend careful interpretation of evaluation-results derived from automatized FAIR assessment. The FAIR principles are a set of high-level principles and applying them depends on the specific context such as discipline-specific aspects. There are various quantitative and qualitative methods to assess the FAIRness of data (see also [FAIRassist.org](https://fairassist.org/) but no definitive methodology (see [Wilkinson et al.](https://doi.org/10.5281/zenodo.7463421)). For this reason, different FAIR assessment tools can provide different scores for the same dataset. We may include alternate, complementary methodologies in future versions of this project. To illustrate the potentials of identifying systematic gaps with automated evaluation approaches, in this dashboard you can observe evaluation results obtained from [F-UJI](https://doi.org/10.5281/zenodo.4063720) as one selected approach. Both, the F-UJI framework and the underlying [metrics](https://doi.org/10.5281/zenodo.6461229) are subject of continuous development. The evaluation results can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but focus on machine-actionable aspects and are limited with respect to human-understandable and discipline-specific aspects of metadata. Evaluations results obtained from F-UJI can be useful in providing guidance for improving the FAIRness of data and repository infrastructure, respectively, but cannot truly assess how FAIR research data really is. 


## Authors

Please find all authors of this project in the CITATION.cff in this repository.

## How to cite this work

**Scientific papers**
- Kubin, M., Sedeqi, M.R., Schmidt, A., Gilein, A., Glodowski, T., Serve, V., Günther, G., Weisweiler, N.L., Preuß, G. and Mannix, O. (2024) "A Data-Driven Approach to Monitor and Improve Open and FAIR Research Data in a Federated Research Ecosystem", Data Science Journal, 23(1), p. 41. Available at: [https://doi.org/10.5334/dsj-2024-041](https://doi.org/10.5334/dsj-2024-041).

**Software publications**
- Preuß, G., Schmidt, A., Gilein, A., Ehlers, P., Glodowski, T., Serve, V., Sedeqi, M. R., Mannix, O., & Kubin, M. (2024). HMC Toolbox for Data Mining (2.0.0). Zenodo. [https://doi.org/10.5281/zenodo.14192982](https://doi.org/10.5281/zenodo.14192982)
- Sedeqi, M. R., Preuß, G., Gilein, A., Glodowski, T., Ehlers, P., Serve, V., Schmidt, A., Mannix, O., & Kubin, M. (2024). HMC FAIR Data Dashboard (2.0.0). Zenodo. [https://doi.org/10.5281/zenodo.14192862](https://doi.org/10.5281/zenodo.14192862)

**Data availability**
- A selective export of the dataset included in the dashboard is published on [Zenodo](https://doi.org/10.5281/zenodo.10890382) and will be updated in regular intervals.

## License

The [HMC FAIR Data Dashboard](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard/hmc-fair-data-dashboard) is licensed under the [Apache License, Version 2.0 (
the "License")](http://www.apache.org/licenses/LICENSE-2.0);
you may not use this file except in compliance with the License.
You can find a copy of the license in the LICENSE-file in this repository.

## Support and Contribution

If you are interested in contributing to the project or have any questions not answered here or in
the documentation, please contact hmc-matter@helmholtz-berlin.de.

### Funding

This work was supported by the [Helmholtz Metadata Collaboration (HMC)](www.helmholtz-metadaten.de), an incubator-platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative. The project was initiated by [HMC Hub Matter](https://helmholtz-metadaten.de/de/materie/team) at [Helmholtz-Zentrum Berlin für Materialien und Energie GmbH (HZB)](https://ror.org/02aj13c28) and was later supported by [HMC Hub Aeronautics, Space and Transport (AST)](https://helmholtz-metadaten.de/en/aeronautics/team) at the [German Aerospace Center (DLR)](https://ror.org/04bwf3e34).

<p>
  <img alt="Logo_HMC" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/blob/main/HMC/HMC_Logo_M.png?raw=true" width="300">

&nbsp;
  <img alt="Logo_HZB" src="https://www.helmholtz-berlin.de/media/design/logo/hzb-logo.svg" width="300">
</p>

&nbsp;
  <img alt="Logo_DLR" src="https://www.dlr.de/en/images/2021/3/dlr-logo-black/@@images/image-2000-f39fe2583e7459827bb9413a233f63b0.jpeg" width="300">
</p>




### Acknowledgements according to [CRediT](https://credit.niso.org/):

With respect to [HMC Dashboard on Open and FAIR Data in Helmholtz](https://codebase.helmholtz.cloud/hmc/hmc-public/FAIR-dashboard), the following individuals are mapped to [CRediT](https://credit.niso.org/) (alphabetical order): \

[Astrid Gilein](https://orcid.org/0000-0003-4167-360X) (AG);
[Alexander Schmidt](https://orcid.org/0009-0005-1368-6114) (AS);
[Gabriel Preuß](https://orcid.org/0000-0002-3968-2446) (GP);
[Mojeeb Rahman Sedeqi](https://orcid.org/0000-0002-9694-0122) (MRS);
[Markus Kubin](https://orcid.org/0000-0002-2209-9385) (MK);
[Oonagh Brendike-Mannix](https://orcid.org/0000-0003-0575-2853)(OBM);
[Pascal Ehlers](https://orcid.org/0009-0008-4039-7977) (PE);
Tempest Glodowski (TG);
[Vivien Serve](https://orcid.org/0000-0001-9603-7630) (VS);

Contributions according to [CRediT](credit.niso.org/) are: \
[Conceptualization](https://credit.niso.org/contributor-roles/conceptualization/): AG, AS, GP, MRS, MK, OBM, VS;
[Data curation](https://credit.niso.org/contributor-roles/data-curation/): AG, AS, GP, MK, PE, TG;
[Methodology](https://credit.niso.org/contributor-roles/methodology/); AG, AS, GP, MRS, MK;
[Project administration](https://credit.niso.org/contributor-roles/project-administration/): MK;
[Software](https://credit.niso.org/contributor-roles/software/): AG, GP, PE, MRS, MK;
[Supervision](https://credit.niso.org/contributor-roles/supervision/): MK, OBM;
[Validation](https://credit.niso.org/contributor-roles/validation/): GP, MRS, PE, MK, VS;
[Visualization](https://credit.niso.org/contributor-roles/visualization/): MRS, MK, VS;
[Writing – original draft](https://credit.niso.org/contributor-roles/writing-original-draft/): MRS, MK, OBM;
[Writing – review & editing](https://credit.niso.org/contributor-roles/writing-review-editing/): MRS, MK, OBM;

